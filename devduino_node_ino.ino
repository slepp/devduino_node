#include <SPI.h>
#include <RF24.h>
#include <Wire.h>
//#include <Digital_Light_TSL2561.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BMP085_U.h>
#include <Adafruit_TSL2561_U.h>
//#include <Sensirion.h>
#include <SHT2x.h>
//#include <TCS3414CS.h>
#include <EEPROM.h>

#include <avr/sleep.h>
#include <avr/wdt.h>

#define LED 9

RF24 rf(8,7);

Adafruit_BMP085_Unified bmp = Adafruit_BMP085_Unified(10085);
Adafruit_TSL2561_Unified tsl = Adafruit_TSL2561_Unified(TSL2561_ADDR_LOW, 12345);

typedef enum { PKT_HELLO = 0, PKT_HEARTBEAT, PKT_START, PKT_TEMPERATURE, PKT_PRESSURE, PKT_LIGHT, PKT_OCCUPANCY, PKT_GENERIC, PKT_STOP } packetType_t;

#define VERSION 0x0101

struct sensorData {
  packetType_t type;
  union {
    struct {
      uint16_t version; // Protocol version
    } hello;
    struct {
      uint32_t counter; // Arbitrary counter of heartbeats
    } heartbeat;
    struct {
      uint8_t  toFollow; // How many frames to expect
      uint32_t ms; // Alive time
    } start;
    struct {
      double temperature; // Temperature
      double humidity;    // Humidity
      double dewpoint;    // Dewpoint
    } temperature;
    struct {
      double pressure;    // Atmospheric pressure in hPa
      float  seaLevel;    // Sea level pressure
      float  temperature; // Temperature
    } pressure;
    struct {
      uint16_t level;      // Light level, lux
      uint8_t  haveColour; // Do we have a colour sensor?
      uint16_t red;       // Value of colours
      uint16_t green;
      uint16_t blue;
      uint16_t clear;
    } light;
    struct {
      uint8_t occupied;
    } occupancy;
    struct { // Generic struct, identified by ID
      uint8_t  id;
      union {
        uint32_t u32;
        uint16_t u16;
        uint8_t  u8;
        double   d;
        float    f;
      } g1, g2, g3, g4, g5;
    } generic;
    struct { // Stop frame
    } stop;
  };
} sd;

uint8_t getPacketSize(uint8_t type) {
  switch(type) {
    case PKT_HELLO: return sizeof(sd.type) + sizeof(sd.hello);
    case PKT_HEARTBEAT: return sizeof(sd.type) + sizeof(sd.heartbeat);
    case PKT_START: return sizeof(sd.type) + sizeof(sd.start);
    case PKT_TEMPERATURE: return sizeof(sd.type) + sizeof(sd.temperature);
    case PKT_PRESSURE: return sizeof(sd.type) + sizeof(sd.pressure);
    case PKT_LIGHT: return sizeof(sd.type) + sizeof(sd.light);
    case PKT_GENERIC: return sizeof(sd.type) + sizeof(sd.generic);
    case PKT_STOP: return sizeof(sd.type) + sizeof(sd.stop);
    default: return 0;
  }
  return 0;
}

// watchdog interrupt
ISR (WDT_vect) 
{
   wdt_disable();  // disable watchdog
}  // end of WDT_vect

double dewPoint(double celsius, double humidity)
{
	// (1) Saturation Vapor Pressure = ESGG(T)
	double RATIO = 373.15 / (273.15 + celsius);
	double RHS = -7.90298 * (RATIO - 1);
	RHS += 5.02808 * log10(RATIO);
	RHS += -1.3816e-7 * (pow(10, (11.344 * (1 - 1/RATIO ))) - 1) ;
	RHS += 8.1328e-3 * (pow(10, (-3.49149 * (RATIO - 1))) - 1) ;
	RHS += log10(1013.246);

        // factor -3 is to adjust units - Vapor Pressure SVP * humidity
	double VP = pow(10, RHS - 3) * humidity;

        // (2) DEWPOINT = F(Vapor Pressure)
	double T = log(VP/0.61078);   // temp var
	return (241.88 * T) / (17.558 - T);
}

// The RF24 code automatically powers down after a write
void sendFrame(bool standby = false) {
  static bool poweredDown = true;
  bool sent = false;
  uint8_t attempts = 0;
  
  if(poweredDown)
    rf.powerUp();
    
  //rf.stopListening();
  sent = rf.write(&sd, getPacketSize(sd.type));
  if(!sent) {
    digitalWrite(LED, HIGH);
    do {
      sent = rf.write(&sd, getPacketSize(sd.type));
    } while(!sent && ++attempts < 5);
    digitalWrite(LED, LOW);
  }
  //rf.startListening();
  
  if(standby) {
    rf.powerDown();
    poweredDown = true;
  }
}

uint8_t nrfAddress[6];

void setup() {
  pinMode(LED, OUTPUT);
  digitalWrite(LED, LOW);
  
  pinMode(A2, INPUT);
  pinMode(A3, INPUT);
  
  // put your setup code here, to run once:
  rf.begin();

  // Set RF parameters
  rf.setPALevel(RF24_PA_HIGH);
  rf.setChannel(98);
  rf.setDataRate(RF24_1MBPS);
  rf.setRetries(8,15);
  
  // Use dynamic payload lengthd
  rf.enableDynamicPayloads();
  
  // Enable the writing pipe from our address
  // Fetch address from EEPROM first
  nrfAddress[0] = EEPROM.read(0);
  nrfAddress[1] = EEPROM.read(1);
  nrfAddress[2] = EEPROM.read(2);
  nrfAddress[3] = EEPROM.read(3);
  nrfAddress[4] = EEPROM.read(4);
  nrfAddress[5] = 0;
  rf.openWritingPipe(nrfAddress);
  rf.stopListening();
  rf.powerDown();  
  
  // Initialize barometric sensor
  bmp.begin();
  
  // Initialize light sensor
  //TSL2561.init();
  tsl.begin();
  tsl.setIntegrationTime(TSL2561_INTEGRATIONTIME_101MS);
  tsl.setGain(TSL2561_GAIN_16X);
  
  // Initialize colour sensor
  //TCS3414CS_init();
  sd.type = PKT_HELLO;
  sd.hello.version = VERSION;
  sendFrame(true);
}

// Read the VCC value
unsigned short readVcc() {
  long result;
  // Read 1.1V reference against AVcc
  ADMUX = _BV(REFS0) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);
  delay(2); // Wait for Vref to settle
  ADCSRA |= _BV(ADSC); // Convert
  while (bit_is_set(ADCSRA,ADSC));
  result = ADCL;
  result |= ADCH<<8;
  result = 1126400L / result; // Back-calculate AVcc in mV
  return (unsigned short)result;
}

void sleepNow() {
  uint8_t old_ADCSRA = ADCSRA;
  ADCSRA = 0;  

  // clear various "reset" flags
  MCUSR = 0;     
  // allow changes, disable reset
  WDTCSR = bit (WDCE) | bit (WDE);
  // set interrupt mode and an interval 
  WDTCSR = bit (WDIE) | bit (WDP3) | bit (WDP0);    // set WDIE, and 2 seconds delay
  wdt_reset();  // pat the dog

  set_sleep_mode (SLEEP_MODE_PWR_DOWN);  
  sleep_enable();
 
  // turn off brown-out enable in software
  MCUCR = bit (BODS) | bit (BODSE);
  MCUCR = bit (BODS); 
  sleep_cpu ();  

  // cancel sleep as a precaution
  sleep_disable();
  ADCSRA = old_ADCSRA;
}
      

enum { S_START = 0, S_HELLO, S_TEMPERATURE, S_PRESSURE, S_LIGHT, S_OCCUPANCY, S_GENERIC, S_SLEEP } state = S_START;

void loop() {
  sensors_event_t event;
    
  switch(state) {
    case S_START:
      // Starting
      sd.type = PKT_START;
      sd.start.toFollow = 4;
      sd.start.ms = millis()/1000;
      //sendFrame();
      state = S_TEMPERATURE;
      break;
    case S_TEMPERATURE:
      sd.type = PKT_TEMPERATURE;
      sd.temperature.temperature = SHT2x.GetTemperature();
      sd.temperature.humidity    = SHT2x.GetHumidity();
      sd.temperature.dewpoint    = dewPoint(sd.temperature.temperature, sd.temperature.humidity);
      sendFrame(true);
      state = S_PRESSURE;
      sleepNow();
      break;
    case S_PRESSURE: 
      bmp.getEvent(&event);
      sd.type = PKT_PRESSURE;
      sd.pressure.pressure = event.pressure;
      bmp.getTemperature(&sd.pressure.temperature);
      sd.pressure.seaLevel = bmp.seaLevelForAltitude(668.0, sd.pressure.pressure, sd.pressure.temperature);
      sendFrame(true);
      state = S_LIGHT;
      sleepNow();
      break; 
    case S_LIGHT:
      tsl.getEvent(&event);
      sd.type = PKT_LIGHT;
      sd.light.level = event.light;
      sd.light.haveColour = 0;
      sd.light.red = 0;
      sd.light.green = 0;
      sd.light.blue = 0;
      sd.light.clear = 0;
      sendFrame(true);
      state = S_GENERIC;
      sleepNow();
      break;
    case S_GENERIC:
      sd.type = PKT_GENERIC;
      // Generic ID is variant 2 of the Vcc readings
      sd.generic.id = 0x02;
      sd.generic.g1.u16 = readVcc();
      {
        analogReference(INTERNAL);
        analogRead(A2);
        delay(10);
        unsigned short a = 0; // 1024 * 8 < 65535, so use short
        for(uint8_t i = 0; i < 8; i++) {
          a += analogRead(A2);
        }
        sd.generic.g2.u16 = a >> 3;
        // The value is the raw value, multiplied by 3440mV max resolution
        // Constant is from: (3440.42552/1024.0)
        sd.generic.g4.f = sd.generic.g2.u16 * 3.35979054;
      }
      {
        // Already switched references above
        //analogReference(INTERNAL);
        //analogRead(A3);
        //delay(10);
        unsigned short a = 0; // 1024 * 8 < 65535, so use short
        for(uint8_t i = 0; i < 8; i++) {
          a += analogRead(A3);
        }
        sd.generic.g3.u16 = a >> 3;
        // Simplified this to only do one multiply and one substraction
        // By multiplying reference by 100.
        // Constant is from 110.0/1024.0
        sd.generic.g5.f = sd.generic.g3.u16*0.10742187;
        sd.generic.g5.f -= 50.0;
      }
      // In this frame, g1 is vcc, g2 is raw vcc2, g3 is raw temp,
      // g4 is float vcc2, g5 is float temperature
      sendFrame(true);
      state = S_SLEEP;
      sleepNow();
      break;
    case S_SLEEP:
      sd.type = PKT_STOP;
      //sendFrame(true);
      state = S_START;
      // disable ADC
      break;
      
    default:
      state = S_START;
      break;
  }
  
//  readRGB();
//  printColor(getColorSingle(2800,2300,2000)); 
}
